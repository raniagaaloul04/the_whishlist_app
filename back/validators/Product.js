const joi = require('@hapi/joi')

const validatproductData = data => {
    const schema = joi.object({


        Title: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
        Description: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
        Status: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
        Price: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
        Currency: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
        Whishlist: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
    })
    return schema.validate(data, { abortEarly: false })

}
module.exports = {
    validatproductData,


}