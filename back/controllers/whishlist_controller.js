const whishlist_model = require("../models/whishlist_model")
const {  validatwhishlistData } = require("../validators/Whishlist");



module.exports = {
    
    add: (req, res) => {

        const { error } =validatwhishlistData(req.body)

        if (error) {
            return res.status(422).json({
                success: false,
                message: 'data is not valid',
                errors: error,
            })
        }

        whishlist_model.findOne({ Name : req.body.Name }, (err, Whishlist) => {
            if ( Whishlist) {
                res.status(422).json({
                    success: false,
                    message: ' Whishlist exist',
                    errors: { details: [{ path: ['Name'], message: ' Whishlist with this name is already exist' }] }
                })
            } else {

                const data = {
             
                   Name: req.body.Name,
                   
                }

               
                whishlist_model.create(data, (err, Whishlist) => {
                    if (err) {
                        res.status(500).json({
                            success: false,
                            message: "error create Whishlist",
                            errors: { details: [{ path: ['global'], message: 'something went wrong' }] }
                        })
                    } else {
                        res.status(201).json({
                            success: true,
                            message: " Whishlist successfuly created",
                            data: Whishlist
                        })
                    }
                })
            }
        })
    },
    GetAllWhishsLists: (req, res) => {
        whishlist_model.find({}).exec((err, listewhishlits) => {
            if (err)
            res.status(500).json({
                message:err,
                statut:500
            })
            else 
            res.status(200).json({
message:'whishlist founded',
statut:200,
data:listewhishlits


            })


        }
        )


    }
}