const express = require('express')
const multer=require('../muddleware/uploads')
const product_controller = require('../controllers/product_controller')

const route = express.Router()

route.post('/add',multer.single('file'),product_controller.add)
route.get('/all',product_controller.GetAllProducts)

module.exports = route