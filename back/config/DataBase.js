// Set Up mongoose connection
const mongoose=require("mongoose")
const db='mongodb://localhost:27017/wishlists'
mongoose.connect(db , ()=> {
    console.log('connected to db');
} );
///
mongoose.Promise=global.Promise;
mongoose.connection.on("error",
console.error.bind(console,"Mongodb connection error"))
module.exports=mongoose;