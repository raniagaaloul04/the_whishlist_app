const product_model = require("../models/product_model")
const { validatproductData } = require("../validators/Product");



module.exports = {
    add: (req, res) => {

        const { error } = validatproductData(req.body)

        if (error) {
            return res.status(422).json({
                success: false,
                message: 'data is not valid',
                errors: error,
            })
        }

        product_model.findOne({ Title: req.body.Title }, (err, product) => {
            if (product) {
                res.status(422).json({
                    success: false,
                    message: 'Product exist',
                    errors: { details: [{ path: ['name'], message: 'Product with this name is already exist' }] }
                })
            } else {

                const data = {
                    image: req.file.filename,
                    Title: req.body.Title,
                    Description: req.body.Description,
                    Status: req.body.Status,
                    Price: req.body.Price,
                    Currency: req.body.Currebcy,
                    Whishlist: req.body.Whishlist
                }

                product_model.create(data, (err, product) => {
                    if (err) {
                        res.status(500).json({
                            success: false,
                            message: "error create product",
                            errors: { details: [{ path: ['global'], message: 'something went wrong' }] }
                        })
                    } else {
                        res.status(201).json({
                            success: true,
                            message: "product successfuly created",
                            data: product
                        })
                    }
                })
            }
        })
    },
    GetAllProducts: (req, res) => {
        product_model.find({}).exec((err, listeproducts) => {
            if (err)
            res.status(500).json({
                message:err,
                statut:500
            })
            else 
            res.status(200).json({
message:'product founded',
statut:200,
data:listeproducts


            })


        }
        )


    }

}