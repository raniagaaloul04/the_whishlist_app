const programmer_model = require("../models/programmer_model")
const {validatesignData} = require("../validators/Programmer");


module.exports = {


    signup: async (req, res) => {
        const { error } =   validatesignData(req.body)

        if (error) {
            return res.json({
                success: false,
                message: 'data is not valid',
                errors: error,
            })
        }

        await programmer_model.findOne({ Name: req.body.Name }, async (err, programmer) => {
            if (programmer) {
                res.json({
                    success: false,
                    message: 'name invalide',
                    errors: { details: [{ path: ['name'], message: 'Programmer with this name is already exist' }] }
                })
            } else {
         

                const data = {
                   Name: req.body.Name,
                   Password: req.body.Password,
                   
                }

                programmer_model.create(data, (err,programmer) => {
                    if (err) {
                        res.json({
                            success: false,
                            message: "error Programmer Sign Up",
                            errors: { details: [{ path: ['Name'], message: err }] }
                        })
                    } else {
                        res.json({
                            success: true,
                            message: "Programmer successfuly Sign Up",
                            data:programmer
                        })
                    }
                })
            }
        })

    },
   signin: async (req, res) => {

        const { error } =   validatesignData(req.body)

        if (error) {
            return res.status(422).json({
                success: false,
                message: "data is not valid ",
                errors: error
            })
        }

        const programmer = await  programmer_model.findOne({ Name: req.body.Name ,Password:req.body.Password})

        if (!programmer) {
            
         
                res.status(403).json({
                    success: false,
                    message: 'invalid Name',
                    errors: { details: { path: ['Name'], message: 'Programmer with this name does not exists' } }
                })
            }

         else {
            res.status(200).json({
                success: true,
                message: 'Succes Sign In',
            })
        }

    }
}